# -*- coding: utf-8 -*-
"""
Created on Sun Feb  4 01:45:18 2018

@author: enovi
"""

import nltk

#Psychological analyzer
def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Psychological Text Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    #Big 5 traits are openness to experience, conscientiousness, extraversion, agreeableness, neuroticism
    o_high       = ['act', 'adventure', 'anticipation', 'arcade', 'bewitch', 'bivouac', 'bliss',
                    'bliss', 'camp', 'cantonment', 'captivate', 'charm', 'concert', 'current',
                    'delight', 'display', 'dream', 'ecstasy', 'elation', 'enchant', 'entertain',
                    'entrance', 'euphoria', 'event', 'excite', 'exhibit', 'exhiliration',
                    'experience', 'feat', 'festival', 'fresh', 'gallery', 'gig', 'gladden',
                    'gratify', 'happy', 'hike', 'imaginative', 'invigor', 'journey', 'joy',
                    'latest', 'latest', 'lively', 'marvel', 'modern', 'museum', 'new', 'novel',
                    'original', 'outing', 'performance', 'piece', 'play', 'pleasure',
                    'presentation', 'production', 'rapture', 'recent', 'recital', 'risk', 'rove',
                    'rush', 'scene', 'sensation', 'show', 'sprightly', 'spry', 'staging', 'tent',
                    'thrill', 'tingle', 'traipse', 'trek', 'vibrant', 'vital', 'zing']
    
    o_low        = ['abnormal', 'alien', 'anomalous', 'atrocious', 'atypical', 'bizarre',
                    'coarse', 'controversial', 'crude', 'danger', 'deviant', 'different',
                    'disgusting', 'evil', 'explicit', 'extraordinary', 'extreme', 'fool', 'freak',
                    'heinous', 'hideous', 'immoral', 'improper', 'indecent', 'infrequent',
                    'irregular', 'isolated', 'lewd', 'objectionable', 'obscene', 'occasional',
                    'odd', 'odious', 'offbeat', 'offensive', 'outlandish', 'outrageous',
                    'peculiar', 'quirk', 'rare', 'repugnant', 'rude', 'salacious', 'shocking',
                    'singular', 'sporadic', 'strange', 'surprising', 'unacceptable',
                    'unconventional', 'uncustomary', 'unexpected', 'unfamiliar', 'unorthodox',
                    'unusual', 'unwonted', 'vulgar', 'wacky', 'weird', 'wicked']
    
    c_high       = ['achieve', 'application', 'assign', 'authority', 'blueprint', 'calling',
                    'career', 'chore', 'commission', 'continue', 'control', 'course', 'creation',
                    'curriculum', 'design', 'diagram', 'direction', 'discipline', 'drill',
                    'drudge', 'duty', 'effort', 'employ', 'exertion', 'flow', 'grind', 'industry',
                    'instruct', 'job', 'labor', 'lecture', 'map', 'meet', 'moil', 'occupation',
                    'order', 'persevere', 'persist', 'plan', 'position', 'practice', 'praxis',
                    'prepare', 'profession', 'project', 'regimen', 'regulate', 'regulation',
                    'research', 'routine', 'schedule', 'scheme', 'service', 'situation', 'skill',
                    'slog', 'strategy', 'study', 'subject', 'sweat', 'system', 'tactic', 'task',
                    'teach', 'timetable', 'toil', 'trade', 'train', 'travail', 'vocation', 'work']
    
    c_low        = ['abandon', 'abjure', 'adjourn', 'boring', 'break', 'breather', 'brunch',
                    'cancel', 'cease', 'cede', 'chaos', 'clutter', 'conclude', 'confusion',
                    'debris', 'defer', 'delay', 'desert', 'detritus', 'dinner', 'disarray',
                    'disheveled', 'disorganize', 'dissolve', 'ditch', 'doze', 'drop', 'drudge',
                    'dull', 'dump', 'finish', 'flee', 'fragments', 'furlough', 'halt', 'hold',
                    'holiday', 'jettison', 'jilt', 'jumble', 'junk', 'lazy', 'leave', 'litter',
                    'lost', 'lunch', 'mess', 'miscellany', 'mishmash', 'missing', 'muddle', 'nap',
                    'pause', 'postpone', 'prorogue', 'recess', 'relinquish', 'rest', 'retreat',
                    'rubbish', 'scatter', 'scrap', 'scrub', 'search', 'shambles', 'shelve',
                    'slack', 'slumber', 'snack', 'snooze', 'sojourn', 'staycation', 'strand',
                    'surrender', 'tangle', 'tour', 'trash', 'trip', 'untidy', 'vacation', 'vacay',
                    'withdraw', 'yield']
    
    e_high       = ['affiliate', 'affinity', 'alliance', 'ally', 'assemblage', 'associate',
                    'band', 'bevy', 'bloc', 'brotherhood', 'bunch', 'chorus', 'circle', 'class',
                    'clique', 'club', 'coalition', 'collective', 'combine', 'common', 'company',
                    'concord', 'concur', 'confidant', 'consensus', 'consent', 'consortium',
                    'couple', 'crew', 'crowd', 'ensemble', 'fraternity', 'friends', 'gaggle',
                    'gang', 'gather', 'group', 'guild', 'harmony', 'horde', 'huddle', 'intimate',
                    'jointly', 'league', 'majority', 'mob', 'multitude', 'network',
                    'organization', 'our', 'pack', 'pact', 'partner', 'party', 'posse', 'ring',
                    'sisterhood', 'society', 'solidarity', 'sorority', 'soulmate', 'squad',
                    'swarm', 'syndicate', 'tandem', 'team', 'throng', 'together', 'treaty',
                    'troupe', 'unanimity', 'union', 'unison', 'unity', 'us', 'we', 'you']
    
    e_low        = ['I', 'abandoned', 'alone', 'anchoress', 'anchorite', 'antisocial', 'ascetic',
                    'avoidant', 'cloistered', 'companionless', 'cutoff', 'deserted', 'desolate',
                    'distant', 'eremite', 'forgotten', 'forlorn', 'forsaken', 'hermit',
                    'hikikomori', 'incommunicado', 'independent', 'individual', 'introvert',
                    'isolated', 'lone', 'me', 'mine', 'misanthrope', 'myself', 'neglected',
                    'outlying', 'partnerless', 'pillarist', 'private', 'recluse', 'remote',
                    'reserved', 'retiring', 'secluded', 'seclusion', 'single', 'sole', 'solitary',
                    'solo', 'solus', 'stranded', 'stylite', 'unaccompanied', 'unaided',
                    'unassisted', 'unattended', 'unescorted', 'unforthcoming', 'unsociable',
                    'vacant', 'withdrawn']
    
    a_high       = ['acclaim', 'acclamation', 'accolade', 'acknowledge', 'address', 'admire',
                    'adore', 'adulation', 'affirm', 'agree', 'allow', 'applaud', 'applause',
                    'appreciate', 'approbation', 'approve', 'authorize', 'award', 'ballyhoo',
                    'bless', 'celebrate', 'cheer', 'clap', 'commend', 'compliment',
                    'congratulate', 'contribute', 'encomium', 'encourage', 'endorse', 'enliven',
                    'eulogy', 'exalt', 'exhilarate', 'flatter', 'gladden', 'glorify', 'greet',
                    'hail', 'hallow', 'hearten', 'hello', 'honor', 'inspire', 'joy', 'kudos',
                    'laud', 'laurel', 'like', 'lionize', 'love', 'motivate', 'ovation',
                    'panegyrize', 'praise', 'proclaim', 'puffery', 'ratify', 'recommend',
                    'rejoice', 'revere', 'reward', 'right', 'salute', 'share', 'support',
                    'treasure', 'tribute', 'uphold', 'uplift', 'validate', 'venerate', 'vote',
                    'welcome']
    
    a_low        = ['afflict', 'afters', 'altercation', 'antagonism', 'argue', 'ban', 'battle',
                    'brawl', 'challenge', 'chastise', 'clash', 'concoct', 'condone', 'conflict',
                    'contention', 'contest', 'controversy', 'cynic', 'deceit', 'disagree',
                    'disallow', 'discord', 'disloyal', 'dispute', 'dissension', 'dissent',
                    'distort', 'donnybrook', 'doubt', 'duplicity', 'expose', 'fabricate', 'fake',
                    'fallacy', 'false', 'feud', 'fickle', 'fight', 'flaw', 'fracas', 'friction',
                    'halt', 'hinder', 'hit', 'imprecise', 'inaccurate', 'incite', 'inconstant',
                    'incorrect', 'miff', 'opposition', 'prevent', 'problem', 'prohibit', 'punish',
                    'quarrel', 'row', 'scrap', 'shady', 'shallow', 'sketchy', 'spat', 'squabble',
                    'stop', 'strife', 'tiff', 'traitor', 'unreliable', 'untrue', 'variance',
                    'wicked', 'wily', 'wrangle', 'wrong']
    
    n_high       = ['agitate', 'alarm', 'antsy', 'anxiety', 'anxious', 'apprehensive', 'bearish',
                    'bother', 'brittle', 'challenge', 'complex', 'concern', 'consequences',
                    'craze', 'cry', 'desperate', 'difficult', 'discombombulate', 'disconcert',
                    'disquiet', 'distraught', 'distress', 'disturb', 'dither', 'edgy', 'escape',
                    'faze', 'fear', 'fever', 'flight', 'fluster', 'frantic', 'fraught', 'frenzy',
                    'fret', 'fright', 'hysteric', 'inflame', 'issue', 'jumpy', 'mad', 'misery',
                    'mousy', 'nervous', 'overwrought', 'panic', 'paranoid', 'perturb', 'question',
                    'rattle', 'restless', 'stress', 'struggle', 'tense', 'threat', 'timid',
                    'tizzy', 'trouble', 'uncontrolled', 'uneasy', 'unhinged', 'unsettle',
                    'unsure', 'victim', 'worry']
    
    n_low        = ['affable', 'aplomb', 'assertive', 'assure', 'backbone', 'bold', 'brave',
                    'bullish', 'buoyant', 'calm', 'casual', 'clear', 'comfort', 'composed',
                    'conclusive', 'confident', 'conviction', 'courage', 'credence', 'cushy',
                    'determined', 'easy', 'easygoing', 'effortless', 'elementary', 'endure',
                    'facile', 'fact', 'faith', 'familiar', 'fearless', 'feisty', 'fortitude',
                    'grit', 'hope', 'informal', 'insouciant', 'mettle', 'natural', 'nonchalant',
                    'optimistic', 'painless', 'peace', 'pluck', 'poise', 'positive', 'relax',
                    'reliance', 'relief', 'rely', 'resilient', 'resolution', 'resolve', 'safe',
                    'sanguine', 'secure', 'serene', 'simple', 'spirit', 'steadfast',
                    'straightforward', 'suave', 'tranquil', 'truth', 'unafraid', 'unceremonious',
                    'unchallenging', 'unconcerned', 'undemanding', 'understand', 'undisturbed',
                    'unflappable', 'unreserved', 'untroubled', 'upbeat', 'urbane', 'valid',
                    'will']
    
    o_high_count = 0
    o_low_count  = 0
    c_high_count = 0
    c_low_count  = 0
    e_high_count = 0
    e_low_count  = 0
    a_high_count = 0
    a_low_count  = 0
    n_high_count = 0
    n_low_count  = 0
    o_score      = 0
    c_score      = 0
    e_score      = 0
    a_score      = 0
    n_score      = 0
    trait_total  = 0
    
    #split text string into individual words for analysis
    article_text = article_text.lower()
    article_text = article_text.split(' ')
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in o_high:
        word = nltk.PorterStemmer().stem(word)
    for word in o_low:
        word = nltk.PorterStemmer().stem(word)
    for word in c_high:
        word = nltk.PorterStemmer().stem(word)
    for word in c_low:
        word = nltk.PorterStemmer().stem(word)
    for word in e_high:
        word = nltk.PorterStemmer().stem(word)
    for word in e_low:
        word = nltk.PorterStemmer().stem(word)
    for word in a_high:
        word = nltk.PorterStemmer().stem(word)
    for word in a_low:
        word = nltk.PorterStemmer().stem(word)
    for word in n_high:
        word = nltk.PorterStemmer().stem(word)
    for word in n_low:
        word = nltk.PorterStemmer().stem(word)
    
    #analyze each trait
    for word in article_text:
        if word in o_high:
            o_high_count += 1
        elif word in o_low:
            o_low_count  += 1
        elif word in c_high:
            c_high_count += 1
        elif word in c_low:
            c_low_count  += 1
        elif word in e_high:
            e_high_count += 1
        elif word in e_low:
            e_low_count  += 1
        elif word in a_high:
            a_high_count += 1
        elif word in a_low:
            a_low_count  += 1
        elif word in n_high:
            n_high_count += 1
        elif word in n_low:
            n_low_count  += 1
            
    #add up total scores for each trait    
    trait_total = (o_high_count + o_low_count + c_high_count + c_low_count + e_high_count +
    e_low_count + a_high_count + a_low_count + n_high_count + n_low_count)
    #this should prevent divide by zero problems
    if trait_total == 0:
        trait_total = 1
    #print scores for each trait
    o_score = (o_high_count - o_low_count) * 100 / trait_total
    print('Openness to experience was {}.'.format(o_score))
    c_score = (c_high_count - c_low_count) * 100 / trait_total
    print('Conscientiousness was {}.'.format(c_score))    
    e_score = (e_high_count - e_low_count) * 100 / trait_total
    print('Extraversion was {}.'.format(e_score))
    a_score = (a_high_count - a_low_count) * 100 / trait_total
    print('Agreeableness was {}.'.format(a_score))
    n_score = (n_high_count - n_low_count) * 100 / trait_total    
    print('Neuroticism was {}.'.format(n_score))
    
main()